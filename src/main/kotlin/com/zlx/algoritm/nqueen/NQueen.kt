package com.zlx.algoritm.nqueen

fun main() {
    val nQueen = NQueen(8)
    nQueen.solve()
}

class NQueen(private val numberOfQueen: Int) {

    private val chessTable = Array(numberOfQueen) { IntArray(numberOfQueen) }

    fun solve() {
        if (setQueens(0)) printResult()
        else println("No solution")
    }

    private fun setQueens(columnIndex: Int): Boolean {
        if (columnIndex == numberOfQueen) return true
        for (rowIndex in 0 until numberOfQueen)
            if (isPlaceValid(rowIndex, columnIndex)) {
                chessTable[rowIndex][columnIndex] = 1
                if (setQueens(columnIndex + 1)) return true
                chessTable[rowIndex][columnIndex] = 0
            }
        return false
    }

    private fun isPlaceValid(rowIndex: Int, columnIndex: Int): Boolean {
        for (i in 0 until columnIndex)
            if (chessTable[rowIndex][i] == 1)
                return false

        var i = rowIndex
        var j = columnIndex
        while (i >= 0 && j >= 0)
            if (chessTable[i--][j--] == 1) return false


        i = rowIndex
        j = columnIndex
        while (i < numberOfQueen && j >= 0)
            if (chessTable[i++][j--] == 1) return false

        return true
    }


    private fun printResult() {
        for (row in chessTable) {
            for (column in row) {
                if (column == 1)
                    print("| O ")
                else
                    print("| - ")
            }
            print("|")
            println()
        }
    }

}