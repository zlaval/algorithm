package com.zlx.algoritm.quicksort

import java.util.*
import kotlin.random.Random
import kotlin.system.measureTimeMillis

fun main() {
    val random = Random.Default
    val array = ArrayList<Int>(100_000)
    for (i in 0..100_000) {
        array.add(random.nextInt(0, 1_000_000))
    }

    array.forEach { print("$it ") }
    println()

    //WarmUp
    //QuickSort(array.toIntArray()).sort()

    //QuickSort
    val quickSort = QuickSort(array.toIntArray())
    var result = IntArray(0)
    val runTimeInMillis = measureTimeMillis {
        result = quickSort.sort()
    }
    result.forEach { print("$it ") }
    println()
    println("Runtime = $runTimeInMillis ms")
    println()

    //Array sort
    val intArray = array.toIntArray()
    val arraySortRuntime = measureTimeMillis {
        Arrays.sort(intArray)
    }
    intArray.forEach { print("$it ") }
    println()
    println("Array was sorted in $arraySortRuntime ms")
    println()

    //List sort (Collections.sort)
    val listSortRuntime = measureTimeMillis {
        array.sort()
    }
    println("List was sorted in $listSortRuntime ms")
}

class QuickSort(private val array: IntArray) {

    fun sort(): IntArray {
        sort(0, array.size)
        return array
    }

    private fun sort(start: Int, end: Int) {
        if (end - start < 2) return
        val pivotIndex = partition(start, end)
        sort(start, pivotIndex)
        sort(pivotIndex + 1, end)
    }

    private fun partition(start: Int, end: Int): Int {
        val pivot = array[start]
        var low = start
        var high = end

        while (low < high) {
            while (low < high && array[--high] >= pivot);
            if (low < high) array[low] = array[high]
            while (low < high && array[++low] <= pivot);
            if (low < high) array[high] = array[low]
        }

        array[high] = pivot
        return high
    }

}