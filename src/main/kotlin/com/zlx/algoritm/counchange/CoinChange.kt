package com.zlx.algoritm.counchange

fun main() {
    val coinChange = CoinChange(15, intArrayOf(1, 4, 7))
    coinChange.solve()
}

class CoinChange(val amount: Int, val coins: IntArray) {

    val dpTable = Array(coins.size + 1) { IntArray(amount + 1) }

    fun solve() {
        for (i in 0 until coins.size)
            dpTable[i][0] = 1

        for (i in 1..coins.size)
            for (j in 1..amount)
                if (coins[i - 1] <= j)
                    dpTable[i][j] = dpTable[i - 1][j] + dpTable[i][j - coins[i - 1]]
                else
                    dpTable[i][j] = dpTable[i - 1][j]

        println("$amount is changeable in ${dpTable[coins.size][amount]} ways")
    }

}