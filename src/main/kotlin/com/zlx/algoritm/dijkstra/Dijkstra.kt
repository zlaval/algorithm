package com.zlx.algoritm.dijkstra

import java.util.*


/**
 * A--3---E
 * | \     \
 * 1  2     7
 * |   \     \
 * ST   C--3--TA
 * |   / \   /
 * 4  5  2  1
 * | /   | /
 * B     D
 */
fun main() {

    val start = Node("START")
    val a = Node("A")
    val b = Node("B")
    val c = Node("C")
    val d = Node("D")
    val e = Node("E")
    val target = Node("TARGET")

    val sa = Edge(start, 1, a)
    start.addNeighbour(sa)
    val sb = Edge(start, 4, b)
    start.addNeighbour(sb)
    val ac = Edge(a, 2, c)
    a.addNeighbour(ac)
    val ae = Edge(a, 3, e)
    a.addNeighbour(ae)
    val et = Edge(e, 7, target)
    e.addNeighbour(et)
    val bc = Edge(b, 5, c)
    b.addNeighbour(bc)
    val cd = Edge(c, 2, d)
    c.addNeighbour(cd)
    val ct = Edge(c, 3, target)
    c.addNeighbour(ct)
    val dt = Edge(d, 1, target)
    d.addNeighbour(dt)

    computeSortestPath(start)
    printResult(target)

}


fun computeSortestPath(startNode: Node) {
    startNode.distanceFromStart = 0
    val queue = PriorityQueue<Node>()
    queue.add(startNode)

    while (!queue.isEmpty()) {
        val actualNode = queue.poll()
        actualNode.adjacencies.forEach {
            val target = it.to
            val newDistance = actualNode.distanceFromStart + it.weight
            if (newDistance < target.distanceFromStart) {
                queue.remove(target)
                target.distanceFromStart = newDistance
                target.previous = actualNode
                queue.add(target)
            }
        }
    }
}

fun printResult(node: Node) {
    print("$node <-- ")
    node.previous?.let {
        printResult(it)
    }
}

data class Node(
    val name: String,
    val adjacencies: MutableList<Edge> = LinkedList(),
    var previous: Node? = null,
    var distanceFromStart: Int = Int.MAX_VALUE
) : Comparable<Node> {

    fun addNeighbour(edge: Edge) {
        adjacencies.add(edge)
    }

    override fun compareTo(other: Node): Int {
        return distanceFromStart.compareTo(other.distanceFromStart)
    }

    override fun toString(): String {
        return "$name($distanceFromStart)"
    }

}

data class Edge(val from: Node, val weight: Int, val to: Node)