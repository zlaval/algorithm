package com.zlx.algoritm.fibonacci

import kotlin.system.measureTimeMillis

fun main() {
    val number = 40
    val fibonacci = Fibonacci()
    val timeWithoutMemoization =
        measureTimeMillis { println("Result (no memoization) is ${fibonacci.withoutMemoization(number)}") }

    val timeWithMemoization =
        measureTimeMillis { println("Result (memoization) is ${fibonacci.withMemoization(number)}") }

    println("Without memoization, calculating $number  fibonacci took $timeWithoutMemoization ms")
    println("With memoization, calculating $number  fibonacci took $timeWithMemoization ms")
}

class Fibonacci {

    private val memoizeTable = HashMap<Int, Long>()

    init {
        memoizeTable[0] = 0
        memoizeTable[1] = 1
    }

    fun withoutMemoization(number: Int): Int {
        if (number == 0 || number == 1) return number
        return withoutMemoization(number - 1) + withoutMemoization(number - 2)
    }

    fun withMemoization(number: Int): Long {
        if (memoizeTable.containsKey(number)) return memoizeTable[number]!!
        memoizeTable[number - 1] = withMemoization(number - 1)
        memoizeTable[number - 2] = withMemoization(number - 2)
        val actualValue = memoizeTable[number - 1]!! + memoizeTable[number - 2]!!
        memoizeTable[number] = actualValue
        return actualValue
    }

}