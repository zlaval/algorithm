package com.zlx.algoritm.travellingsalesman

import kotlin.system.measureTimeMillis

fun main() {
    for (i in 0..100) {
        Repository.addCity(City())
    }
    val simulatedAnnealing = SimulatedAnnealing()
    val runtime = measureTimeMillis {
        simulatedAnnealing.solve()
    }
    println()
    println("Simulation runtime: $runtime ms")
}

class SimulatedAnnealing {

    private var bestSolution: SingleTour = SingleTour(Repository.cities)

    fun solve() {
        println("Initial solution = ${bestSolution.calculateDistance()}")
        var temp = 10_000.0
        val coolingRate = 0.003
        var currentSolution = bestSolution
        while (temp > 1) {
            val newSolution = SingleTour(currentSolution.tour)
            val rndOne = (newSolution.tour.size * Math.random()).toInt()
            val rndTwo = (newSolution.tour.size * Math.random()).toInt()

            val cityOne = newSolution.tour[rndOne]
            val cityTwo = newSolution.tour[rndTwo]

            newSolution.tour[rndOne] = cityTwo
            newSolution.tour[rndTwo] = cityOne

            val currentDistance = currentSolution.calculateDistance()
            val newDistance = newSolution.calculateDistance()

            if (acceptanceProbability(currentDistance, newDistance, temp) > Math.random()) {
                currentSolution = SingleTour(newSolution.tour)
            }

            if (currentSolution.calculateDistance() < bestSolution.calculateDistance()) {
                bestSolution = SingleTour(currentSolution.tour)
            }

            temp *= 1 - coolingRate
        }

        println("Best approximated solution = ${bestSolution.calculateDistance()}")
        bestSolution.tour.forEach { print("$it ->") }

    }

    private fun acceptanceProbability(currentDistance: Double, newDistance: Double, temp: Double): Double {
        if (currentDistance > newDistance) return 10.0
        return Math.exp((currentDistance - newDistance) / temp)
    }

}

class SingleTour(private val singleTour: List<City>) {

    private var distance: Double = 0.0
    val tour: MutableList<City> = ArrayList(singleTour)

    fun calculateDistance(): Double {
        if (distance == 0.0) {
            for (i in 0 until tour.size) {
                val from = tour[i]
                val to = if (i + 1 < tour.size) {
                    tour[i + 1]
                } else {
                    tour[0]
                }
                distance += from.distanceTo(to)
            }
        }
        return distance
    }

}

object Repository {

    val cities = ArrayList<City>()

    fun addCity(city: City) {
        cities.add(city)
    }

}


data class City(val x: Int, val y: Int) {

    constructor() : this((Math.random() * 100).toInt(), (Math.random() * 100).toInt())

    fun distanceTo(city: City): Double {
        val xDistance = Math.abs(x - city.x)
        val yDistance = Math.abs(y - city.y)
        return Math.sqrt((xDistance * xDistance) + (yDistance * yDistance).toDouble())
    }
}