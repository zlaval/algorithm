package com.zlx.algoritm.dsf

import java.util.*

/******************
 *        R       *
 *       / \      *
 *      /   \     *
 *     D    C     *
 *    / \  / \    *
 *   B  A  E G    *
 *  /         \   *
 * F          H   *
 *****************/
fun main() {

    val root = Vertex("R")
    val vertex1 = Vertex("A")
    val vertex2 = Vertex("B")
    val vertex3 = Vertex("C")
    val vertex4 = Vertex("D")
    val vertex5 = Vertex("E")
    val vertex6 = Vertex("F")
    val vertex7 = Vertex("G")
    val vertex8 = Vertex("H")
    root.addNeighbour(vertex4)
    root.addNeighbour(vertex3)
    vertex4.addNeighbour(vertex2)
    vertex4.addNeighbour(vertex1)
    vertex3.addNeighbour(vertex5)
    vertex2.addNeighbour(vertex6)
    vertex3.addNeighbour(vertex7)
    vertex7.addNeighbour(vertex8)

    dsf(root)
}

var counter = 0

fun dsf(root: Vertex) {
    counter += 4
    val arrow = if (root.hasChildren()) "->" else "  "
    println("$root $arrow".padStart(counter))

    root.visited = true
    root.neighbours.forEach {
        if (!it.visited) {
            dsf(it)
        }
    }

    counter -= 4
}

data class Vertex(
    val name: String,
    var visited: Boolean = false,
    val neighbours: MutableList<com.zlx.algoritm.dsf.Vertex> = LinkedList()
) {
    fun addNeighbour(vertex: Vertex) {
        neighbours.add(vertex)
    }

    override fun toString(): String {
        return name
    }

    fun hasChildren(): Boolean = neighbours.isNotEmpty()

}