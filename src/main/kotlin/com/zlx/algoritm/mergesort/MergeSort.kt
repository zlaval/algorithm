package com.zlx.algoritm.mergesort


import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*
import kotlin.random.Random
import kotlin.system.measureTimeMillis

fun main() {
    val random = Random.Default
    val array = ArrayList<Int>(100_000)
    for (i in 0..100_000) {
        array.add(random.nextInt(0, 1_000_000))
    }

    val mergeSort = MergeSort(array.toIntArray())
    var result = IntArray(0)
    val runTimeInMillis = measureTimeMillis {
        result = mergeSort.mergeSort()
    }
    result.forEach { print("$it ") }
    println()
    println("Sequential mergesort was finised in $runTimeInMillis ms")
    println()

    val parallelMergeSort = ParallelMergeSort(array.toIntArray())
    var parallelResult = IntArray(0)
    val paralellRunTimeInMillis = measureTimeMillis {
        parallelResult = parallelMergeSort.parallelMergeSort()
    }
    parallelResult.forEach { print("$it ") }
    println()
    println("Parallel mergesort was finised in $paralellRunTimeInMillis ms")
    println()

    val coroutineMergeSort = CoroutineMergeSort(array.toIntArray())
    var coroutineResult = IntArray(0)
    val coroutineRunTimeInMillis = measureTimeMillis {
        coroutineResult = coroutineMergeSort.coroutineMergeSort()
    }
    coroutineResult.forEach { print("$it ") }
    println()
    println("Coroutine mergesort was finised in $coroutineRunTimeInMillis ms")
}

class MergeSort(private val array: IntArray) {

    private var tmp = IntArray(array.size)

    fun mergeSort(): IntArray {
        sort(0, array.size - 1)
        return array
    }

    private fun sort(start: Int, end: Int) {
        if (start >= end) return
        val middle = (start + end) / 2
        sort(start, middle)
        sort(middle + 1, end)
        merge(start, middle, end)
    }

    private fun merge(start: Int, middle: Int, end: Int) {
        if (array[middle] <= array[middle + 1]) return
        tmp = array.copyOf()
        var low = start
        var high = middle + 1
        var actual = low
        while (low <= middle && high <= end) {
            if (tmp[low] <= tmp[high]) array[actual] = tmp[low++]
            else array[actual] = tmp[high++]
            actual++
        }
        while (low <= middle) array[actual++] = tmp[low++]
        while (high <= end) array[actual++] = tmp[high++]
    }

}

class ParallelMergeSort(private val array: IntArray) {

    private var tmp = IntArray(array.size)

    fun parallelMergeSort(): IntArray {
        val workerThread = mergeSort(0, array.size - 1, Runtime.getRuntime().availableProcessors())
        workerThread.start()
        workerThread.join()
        return array
    }

    fun mergeSort(start: Int, end: Int, threads: Int): Thread {
        return Thread { divide(start, end, threads / 2) }
    }

    fun divide(start: Int, end: Int, threads: Int) {
        if (threads <= 1) {
            sort(start, end)
            return
        }
        val middle = (start + end) / 2
        val left = mergeSort(start, middle, threads)
        val right = mergeSort(middle + 1, end, threads)
        left.start()
        right.start()
        left.join()
        right.join()
        merge(start, middle, end)
    }

    private fun sort(start: Int, end: Int) {
        if (start >= end) return
        val middle = (start + end) / 2
        sort(start, middle)
        sort(middle + 1, end)
        merge(start, middle, end)
    }

    private fun merge(start: Int, middle: Int, end: Int) {
        if (array[middle] <= array[middle + 1]) return
        tmp = array.copyOf()
        var low = start
        var high = middle + 1
        var actual = low
        while (low <= middle && high <= end) {
            if (tmp[low] <= tmp[high]) array[actual] = tmp[low++]
            else array[actual] = tmp[high++]
            actual++
        }
        while (low <= middle) array[actual++] = tmp[low++]
        while (high <= end) array[actual++] = tmp[high++]
    }

}

class CoroutineMergeSort(private val array: IntArray) {

    private var tmp = IntArray(array.size)

    fun coroutineMergeSort(): IntArray {
        mergeSort(0, array.size - 1)
        return array
    }

    fun mergeSort(start: Int, end: Int) = runBlocking {
        sort(start, end)
    }

    private suspend fun sort(start: Int, end: Int) {
        if (start >= end) return
        val middle = (start + end) / 2
        val left = GlobalScope.launch { sort(start, middle) }
        val right = GlobalScope.launch { sort(middle + 1, end) }
        left.join()
        right.join()
        merge(start, middle, end)
    }

    private suspend fun merge(start: Int, middle: Int, end: Int) {
        if (array[middle] <= array[middle + 1]) return
        tmp = array.copyOf()
        var low = start
        var high = middle + 1
        var actual = low
        while (low <= middle && high <= end) {
            if (tmp[low] <= tmp[high]) array[actual] = tmp[low++]
            else array[actual] = tmp[high++]
            actual++
        }
        while (low <= middle) array[actual++] = tmp[low++]
        while (high <= end) array[actual++] = tmp[high++]
    }

}