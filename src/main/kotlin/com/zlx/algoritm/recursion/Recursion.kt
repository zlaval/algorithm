package com.zlx.algoritm.recursion

fun main() {
    println(factorial(5))
    println(factorialWithLoop(5))
    println(tailRecFactorial(5))
}

fun factorial(n: Int): Int {
    return if (n == 1) 1
    else n * factorial(n - 1)
}

fun factorialWithLoop(n: Int): Int {
    var number = n
    var result = 1
    while (number > 0) {
        result *= number--
    }
    return result
}

fun tailRecFactorial(number: Int): Int {
    tailrec fun solve(acc: Int, n: Int): Int {
        if (n == 0) return acc
        return solve(acc * n, n - 1)
    }
    return solve(1, number)
}